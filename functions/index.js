"use strict";
const functions = require("firebase-functions");
const nodemailer = require("nodemailer");

/** почта с которой пойдёт отправка */
const gmailEmail = "gmailEmail";
/** пароль */
const gmailPassword = "gmailPassword";
const mailRecipients = ["m.v.cherepanov@gmail.com"];
const mailTransport = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});
exports.helloWorld = functions.https.onRequest((request, response) => {
  response.send("Hello from Firebase!");
});
exports.createMail = functions.firestore
  .document("sendMail/{sendMailId}")
  .onCreate((snap) => {
    const newValue = snap.data();
    var mailOptions = {
      from: gmailEmail,
      to: mailRecipients,
    };
    mailOptions.subject = "Заявка на добавление места";
    let html = `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
         <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
          <title>Ментальная карта Перми</title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        </html><body>#body</body>`;
    let msg =
      "<h2><strong>Тема: </strong>#thema</h2><div><strong>Описание: </strong>#description</div><div>UID пользователя: #uid</div>";
    msg = msg
      .replace("#thema", newValue.Thema)
      .replace("#description", newValue.Description)
      .replace("#uid", newValue.Uid);
    html = html.replace("#body", msg);
    mailOptions.html = html;
    mailTransport
      .sendMail(mailOptions)
      .then(() => {
        console.log("Успешно");
        return null;
      })
      .catch((error) => {
        console.log("Alarm", error);
      });

    return null;
  });
