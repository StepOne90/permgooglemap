import { Component, OnInit, Input ,TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { loginIcon } from '../services/account-service/account.service'
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DALServise, MailModel } from '../services/DAL-Servise/dalservise.service';
import { AccountService, UsreInfoModel } from '../services/account-service/account.service';

@Component({
  selector: 'app-add-place-bid',
  templateUrl: './add-place-bid.component.html',
  styleUrls: ['./add-place-bid.component.css']
})
export class AddPlaceBidComponent implements OnInit {
  description: string = "";
  sendError:boolean=false;
  @Input() thema: string;
  @Input() question: string;
  AddForm: FormGroup;
  modalRef: BsModalRef;
  constructor(public bsModalRef: BsModalRef, private dalService: DALServise, private accountService: AccountService,private modalService: BsModalService) { }

  ngOnInit() {
    this.AddForm = new FormGroup({
      'descriptionFrom': new FormControl(this.description, [
        Validators.required
      ])
    })
  }
  get validDescription() { return this.AddForm.get('descriptionFrom'); }
  sendMail(template: TemplateRef<any>) {
    let user = this.accountService.getUser(true);
    if (!user.Uid) return;
    let mail: MailModel = {
      Description: this.AddForm.get('descriptionFrom').value,
      Thema: this.thema,
      Uid: user.Uid
    }
    this.dalService.AddMail(mail).subscribe((result) => {
      if (result){
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
        this.bsModalRef.hide();
      }
      else{
        this.sendError=true;
        this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
        //this.bsModalRef.hide();
      }
    })
  }
  ok(): void {
    this.modalRef.hide();
  }
  cancel(){
    this.bsModalRef.hide();
  }
}