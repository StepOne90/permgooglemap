import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlaceBidComponent } from './add-place-bid.component';

describe('AddPlaceBidComponent', () => {
  let component: AddPlaceBidComponent;
  let fixture: ComponentFixture<AddPlaceBidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlaceBidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlaceBidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
