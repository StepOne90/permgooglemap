import { Component, HostListener } from '@angular/core';
import { resource } from 'selenium-webdriver/http';
import { AppSettings, AppSettingsModel, MapModel, TypeMarker } from './app.settings'
import { AccountService, loginIcon } from './services/account-service/account.service'
import { auth } from 'firebase/app';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddPlaceBidComponent } from './add-place-bid/add-place-bid.component'
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
//import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appSettings: AppSettingsModel;
  bsModalRef: BsModalRef;
  showMwnu: boolean = true;
  isMobile: boolean = false;
  constructor(private appSetting: AppSettings, public accountService: AccountService, private modalService: BsModalService, private router: Router) {
    this.appSettings = appSetting;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (this.isMobile)
          this.appSettings.SHOWMENU = false;
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
  }
  ngOnInit() {
    this.isMobile = window.innerWidth < 768;
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isMobile = window.innerWidth < 768;
    if (!this.isMobile)
      this.appSettings.SHOWMENU = true;
  }

  title = 'permgooglemap';
  callBackAuth = (result: firebase.auth.UserCredential) => {
    var e = result;
  }
  loginIcons: loginIcon[] = this.accountService.loginIcons;
  getTypeMap(): number {
    let r = this.router.url;
    let param = r.split("/")[2];
    let mapTypes = this.appSetting.getSettings().MAPTYPE.filter(type => { return type.Name == param });
    if (mapTypes && mapTypes.length != 1) {
      return 0;
    }
    if (mapTypes[0].MapTypeMarker == TypeMarker.circle)
      return 1;
    return 0;
  }
  addPlaceUser() {
    let user = this.accountService.getUser(true);
    if (!this.accountService.isAuth()) return;
    let r = this.router.url;
    let param = r.split("/")[2];
    let mapTypes = this.appSetting.getSettings().MAPTYPE.filter(type => { return type.Name == param });
    if (mapTypes && mapTypes.length > 0) {
      let mapType = mapTypes[0];
      let initialState = {
        thema: mapType.MapQuestion,
        question: mapType.MapDescription
      }
      this.bsModalRef = this.modalService.show(AddPlaceBidComponent, { 'class': 'modal-lg', initialState: initialState });
    }

  }
}

