/// <reference types="@types/googlemaps" />
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
const FIELDS = ['photos', 'formatted_address', 'name', 'rating']
@Injectable({
  providedIn: 'root'
})
export class PlaceServiceService {
  places: google.maps.places.PlacesService;

  constructor() {
    //this.places = new google.maps.places.PlacesService();  
  }
  init(divElem:HTMLDivElement){
    this.places = new google.maps.places.PlacesService(divElem);  
  }
  place(placeIdRequest: string): Observable<google.maps.places.PlaceResult> {
    return Observable.create((observer: Observer<google.maps.places.PlaceResult>) => {
      // Invokes geocode method of Google Maps API geocoding.
      this.places.getDetails({ placeId:placeIdRequest}, (
        (results: google.maps.places.PlaceResult, status: google.maps.places.PlacesServiceStatus) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('Place service: geocoder failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    });
  }
}