import { TestBed } from '@angular/core/testing';

import { PlaceServiceService } from './geolocation-service.service';

describe('GeolocationServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceServiceService = TestBed.get(PlaceServiceService);
    expect(service).toBeTruthy();
  });
});
