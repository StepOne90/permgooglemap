import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
const namePlaceSelectCollection = "placeSelect";
const namePlaceSelectUserCollection = "SelectUsers";
const nameDistrictSelectColection = "districtSelect";
const nameSendMailCollection = "sendMail";
import { HttpClient } from '@angular/common/http';
import { UsreInfoModel } from '../account-service/account.service'


@Injectable({
  providedIn: 'root'
})
export class DALServise {
  _cahePolygonDistrict: DistrictPolygonModel = {};
  isBlockSenderProcess: boolean = false;
  constructor(private dbService: AngularFirestore, private http: HttpClient) {
    const settings = {
      timestampsInSnapshots: true
    };
    dbService.firestore.app.firestore().settings(settings);
  }
  //Добавление голоса пользователя
  AddSenderUserByPLace(docId: string, user: UsreInfoModel, colectionName: string, resultSelect?: boolean): Observable<SelectUsersModel> {
    return Observable.create((observer: Observer<SelectUsersModel>) => {
      if (this.isBlockSenderProcess)
        return;
      if (!docId || docId == "") {
        console.log("не указан docId");
        return;
      }
      if (!user.Uid) {
        console.log("не авторизованный пользователь");
      }
      //let eMail = this.accountService.UserInfo.eMail;
      let date = new Date();
      let userTimezoneOffset = date.getTimezoneOffset() * 60000;
      let selectUser: SelectUsersModel = {
        DataIsert: date.getTime() - userTimezoneOffset,
        Uid: user.Uid,
        ResultSelect: resultSelect || null
      }
      this.isBlockSenderProcess = !user.IsAdmin;
      this.dbService.collection(colectionName).doc(docId).collection(namePlaceSelectUserCollection).add(selectUser).then(() => {
        observer.next(selectUser);
      }, () => {
        observer.error("Ошибка при добавлении голоса");
      }).finally(() => {
        observer.complete();
        this.isBlockSenderProcess = false;
      })
    })
  }
  //Добавление нового места. Маркер
  async AddNewPlaceSelectWithMarker(marker: Marker, typeMapId: string, user: UsreInfoModel): Promise<string> {
    if (!user.Uid) return;
    return this.AddNewPlaceSelect(marker.lat, marker.lng, typeMapId, user, marker.countAddUserSelect);
  }
  async AddNewPlaceSelect(lat: number, lng: number, typeMap: string, user: UsreInfoModel, countAddUserSelect?: number): Promise<string> {
    var newPlace: PlaceSelectModel = {
      MarkerTypeId: typeMap,
      Lat: lat,
      Lng: lng
    }
    let docId: string;
    await this.dbService.collection(namePlaceSelectCollection).add(newPlace).then(doc => {
      countAddUserSelect = countAddUserSelect && countAddUserSelect > 0 ? countAddUserSelect : 0;
      for (let i = 0; i < countAddUserSelect; i++) {
        this.AddSenderUserByPLace(doc.id, user, namePlaceSelectCollection).subscribe(() => {

        })
      }
      docId = doc.id;
    });
    return Promise.resolve(docId);
  }
  //проголосовавшие пользователи
  GetSenderUsersByMarker(docId: string, collectionNmae: string): Observable<SelectUsersModel[]> {
    return this.dbService.collection(collectionNmae).doc(docId).collection<SelectUsersModel>(namePlaceSelectUserCollection).get()
      .pipe(map(responce => {
        return responce.docs.map(user => {
          let data = user.data();
          let su: SelectUsersModel = {
            Uid: data.Uid,
            DataIsert: data.DataIsert,
            ResultSelect: data.ResultSelect
          }
          return su;
        })
      }))
  }
  //полигоны для районов
  GetDistrictPolygon(searchQery: string, placeId: string): Observable<Array<google.maps.LatLng[]>> {
    let url = "https://nominatim.openstreetmap.org/search?format=json&q=#q&polygon_geojson=1&limit=1";
    return Observable.create((observer: Observer<Array<google.maps.LatLng[]>>) => {
      if (this._cahePolygonDistrict[placeId] && this._cahePolygonDistrict[placeId].length > 0) {
        observer.next(this._cahePolygonDistrict[placeId]);
        observer.complete();
      } else {
        this.http.get(url.replace("#q", searchQery.replace("р-н", "район"))).subscribe((respoce) => {
          let t = respoce[0].geojson;
          // if (t.type == "Polygon") {
          this._cahePolygonDistrict[placeId] = [];
          t.coordinates.forEach(coordinate => {
            let mapArray = t.type == "Polygon" ? coordinate : coordinate[0];
            this._cahePolygonDistrict[placeId].push(mapArray.map(item => { return new google.maps.LatLng(item[1], item[0]) }));
          });
          observer.next(this._cahePolygonDistrict[placeId]);
          observer.complete();
        })
      }
    })
  }
  //все маркеры по типу
  GetCountMarkersBymarkerType(markerTypeId: string): Observable<PlaceSelectModel[]> {
    return this.dbService.collection<PlaceSelectModel>(namePlaceSelectCollection, ref => ref.where('MarkerTypeId', '==', markerTypeId)).get().pipe(map(docInfo => {
      return docInfo.docs.map(doc => {
        let docData = doc.data()
        let placeSelect: PlaceSelectModel = {
          DocId: doc.id,
          MarkerTypeId: docData.MarkerTypeId || docData.TypeMapId,
          Lat: docData.Lat,
          Lng: docData.Lng,
          SelectUsers: [],
          Description: docData.Description,
          Name: docData.Name
        }
        return placeSelect;
      })
    }))
  }
  //получить голоса пользователей по районам
  GetDistrictSelectUsers(markerTypeId: string, placeId: string): Observable<ResultInitDistrictModel> {
    return Observable.create((observer: Observer<ResultInitDistrictModel>) => {
      this.dbService.collection(nameDistrictSelectColection, ref => ref.where('MarkerTypeId', '==', markerTypeId).where("PlaceId", '==', placeId)).get().subscribe(
        (result) => {
          let resultInit: ResultInitDistrictModel = {
            docId: null,
            SelectUsers: []
          }
          if (result.docs.length == 1) {//если так всё ок
            let doc = result.docs[0];
            resultInit.docId = doc.id;
            this.GetSenderUsersByMarker(doc.id, nameDistrictSelectColection).subscribe(r => {
              resultInit.SelectUsers = r;
              observer.next(resultInit);
              observer.complete();
            })
          } else {
            observer.next(resultInit);
            observer.complete();
          }
        }
      )
    })
  }
  isBlockSenderDistrict: boolean = false;
  public AddSenderDistrictUserShared(docId: string, user: UsreInfoModel, placeId: string, markerTypeId: string, resultSelect: boolean): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      this.isBlockSenderDistrict = !user.IsAdmin;
      this.AddSenderDistrictUser(docId, user, placeId, markerTypeId, resultSelect).subscribe((result) => {
        this.isBlockSenderDistrict = false;
        observer.next(result);
        observer.complete();
      },
        (error) => { this.isBlockSenderDistrict = false; observer.error(error) })
    })
  }
  private AddSenderDistrictUser(docId: string, user: UsreInfoModel, placeId: string, markerTypeId: string, resultSelect: boolean): Observable<any> {
    return Observable.create((observer: Observer<any>) => {
      if (!user.Uid || !placeId) {
        observer.error("Не авторизованны или не известное место");
        return;
      }
      let _docId = docId || null;
      if (!_docId) {
        this.dbService.collection(nameDistrictSelectColection, ref => ref.where('MarkerTypeId', '==', markerTypeId).where("PlaceId", '==', placeId)).get()
          .subscribe(
            (result) => {
              if (result.docs.length == 0) {//создаем новый
                let newDistrictSender: DistrictSelectModel = {
                  MarkerTypeId: markerTypeId,
                  PlaceId: placeId
                }
                this.dbService.collection(nameDistrictSelectColection).add(newDistrictSender).then(doc => {
                  this.AddSenderUserByPLace(doc.id, user, nameDistrictSelectColection, resultSelect).subscribe((result) => {
                    observer.next(result);
                    observer.complete();
                  });
                })
              }
              if (result.docs.length == 1) {//добавляем к существующему
                this.AddSenderUserByPLace(result.docs[0].id, user, nameDistrictSelectColection, resultSelect).subscribe((result) => {
                  observer.next(result);
                  observer.complete();
                });
              }
              if (result.docs.length > 1) {//ошибка в БД 
                observer.error("в БД больше одной записи по данному раону");
              }
            },
            (error) => { observer.error(error); },
          );
      } else {
        this.AddSenderUserByPLace(_docId, user, nameDistrictSelectColection, resultSelect).subscribe(
          (result) => {
            observer.next(result);
            observer.complete();
          },
          (error) => { observer.error(error); }
        )
      }
    })

  }
  GetAdminByUid(uid: string): Observable<boolean> {
    return this.dbService.collection("admins", ref => ref.where('uid', '==', uid)).get().pipe(map(result => {
      return result.size > 0;
    }))
  }
  UpdateMetaPlace(docId: string, marker: Marker): void {
    if (!docId) return;
    marker.name = marker.name || null;
    marker.description = marker.description || null;
    /*
    let m: PlaceSelectModel = {
      Description: marker.description,
      Lat: marker.lat,
      Lng: marker.lng,
      MarkerTypeId: marker.typeId,
      SelectUsers: marker.selectUsers,
      Name: marker.name
    };*/
    this.dbService.collection(namePlaceSelectCollection).doc(docId).update({ Name: marker.name, Description: marker.description });
  }
  RemovePlace(marker: Marker):void{
    if(!marker.docId) return;    
    this.dbService.collection(namePlaceSelectCollection).doc(marker.docId).delete();      
  }
  AddMail(newMail: MailModel): Observable<boolean> {
    return Observable.create((observer: Observer<any>) => {
      this.dbService.collection(nameSendMailCollection).add(newMail).then((responce) => {
        observer.next(true);
        observer.complete();
      }).catch((error) => {
        observer.next(false);
        observer.error("Ошибка ")
      })

    })
  }

}
export interface MailModel {
  Uid: string;
  Thema: string;
  Description: string
}
export interface SelectUsersModel {
  Uid?: string,
  DataIsert: number,
  ResultSelect?: boolean
}
interface ResultInitDistrictModel {
  docId: string;
  SelectUsers: SelectUsersModel[],
}
export interface PlaceSelectModel {
  DocId?: any,
  SelectUsers?: SelectUsersModel[],
  MarkerTypeId: string,
  Lat: number,
  Lng: number,
  Name?: string,
  Description?: string
}
interface DistrictSelectModel {
  SelectUsers?: SelectUsersModel[],
  MarkerTypeId: string,
  PlaceId: string,
}
export interface Marker {
  docId?: string;
  selectUsers: SelectUsersModel[];
  lat: number;
  lng: number;
  radius: number;
  contentInfoWindowPlace?: google.maps.places.PlaceResult[];
  contentInfoWindowDistrict?: string;
  isOpenInfoWindow?: boolean;
  color: string;
  districtPolygons?: Array<google.maps.LatLng[]>;
  placeId?: string;
  typeId: string;
  isEdit?: boolean;
  name: string;
  description: string;
  countAddUserSelect?: number;
}
export interface DistrictPolygonModel {
  [placeId: string]: Array<google.maps.LatLng[]>
}