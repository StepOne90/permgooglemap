import { TestBed } from '@angular/core/testing';

import { DALServise } from './dalservise.service';

describe('DALServiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DALServise = TestBed.get(DALServise);
    expect(service).toBeTruthy();
  });
});
