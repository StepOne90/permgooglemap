import { TestBed } from '@angular/core/testing';

import { GeocodingServiceService } from './geocoding-service.service';

describe('GeocodingServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeocodingServiceService = TestBed.get(GeocodingServiceService);
    expect(service).toBeTruthy();
  });
});
