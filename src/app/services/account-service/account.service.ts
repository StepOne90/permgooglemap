import { Injectable, Component, OnInit } from '@angular/core';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { DALServise } from '../DAL-Servise/dalservise.service'
import { LoginUserModalComponent } from './login-user-modal/login-user-modal.component'
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  public AccountData: firebase.auth.UserMetadata
  private user: firebase.User;
  bsModalRef: BsModalRef;
  public UserInfo: UsreInfoModel = {
    DisplayName: null,
    authMethod: null,
    eMail: null,
    IsAdmin: false,
    Uid: null
  }
  public IsAuth: boolean = false;
  constructor(public afAuth: AngularFireAuth, private dalService: DALServise, private modalService: BsModalService) {
    afAuth.user.subscribe(
      result => {
        if (!result) {
          this.clearUserInfo();
        } else {
          this.IsAuth = true;
          this.user = result;
          this.UserInfo.DisplayName = this.user.displayName;
          this.UserInfo.eMail = this.user.email;
          this.UserInfo.Uid = this.user.uid;
          if (result.providerData[0].providerId.match(/google/gm)) {
            this.UserInfo.authMethod = "/assets/google.svg";
          }
          this.dalService.GetAdminByUid(result.uid).subscribe(result => this.UserInfo.IsAdmin = result);
        }
      }
    );
  }
  vkLogin(nameIcon: string) {
    this.UserInfo.authMethod = nameIcon;
  }
  googleLogin() {
    this.afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }
  facebookLogin() {
    this.afAuth.auth.signInWithPopup(new auth.FacebookAuthProvider());
  }
  isAdmin() {
    return this.UserInfo.IsAdmin;
  }
  getUser(chekAuth?): UsreInfoModel {
    chekAuth = chekAuth || false;
    let initialState = {
      loginIcons: this.loginIcons
    };
    if (!this.IsAuth && chekAuth)
      this.bsModalRef = this.modalService.show(LoginUserModalComponent, { 'class': 'modal-sm', initialState: initialState });
    return this.UserInfo;
  }
  isAuth() {
    return this.IsAuth;
  }
  logOut() {
    this.afAuth.auth.signOut();
  }
  clearUserInfo() {
    this.IsAuth = false;
    this.UserInfo = {
      DisplayName: null,
      IsAdmin: false,
      Uid: null,
      authMethod: null,
      eMail: null
    }
  }
  loginIcons: loginIcon[] = [
   {
      iconName: "/assets/google.svg",
      iconClick: () => {
        this.googleLogin();
      }
    }, {
      iconName: "/assets/fb.svg",
      iconClick: () => { this.facebookLogin(); }
    }
  ];
}

export class UsreInfoModel {
  eMail: string;
  DisplayName: string;
  authMethod: string;
  IsAdmin: boolean;
  Uid: string;
}
export interface loginIcon {
  iconName: string,
  iconClick: Function
}
