import { Component, OnInit, Input } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { loginIcon } from '../account.service'


@Component({
  selector: 'app-login-user-modal',
  templateUrl: './login-user-modal.component.html',
  styleUrls: ['./login-user-modal.component.css']
})
export class LoginUserModalComponent implements OnInit {
  @Input() loginIcons: loginIcon;

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }
  clickIcon(callback:Function){
    callback();
    this.bsModalRef.hide();
  }

}
