import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginUserModalComponent } from './login-user-modal.component';

describe('LoginUserModalComponent', () => {
  let component: LoginUserModalComponent;
  let fixture: ComponentFixture<LoginUserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginUserModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginUserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
