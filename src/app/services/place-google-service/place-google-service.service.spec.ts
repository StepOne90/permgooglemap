import { TestBed } from '@angular/core/testing';

import { PlaceGoogleServiceService } from './place-google-service.service';

describe('PlaceGoogleServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlaceGoogleServiceService = TestBed.get(PlaceGoogleServiceService);
    expect(service).toBeTruthy();
  });
});
