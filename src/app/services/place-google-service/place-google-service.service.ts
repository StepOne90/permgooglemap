/// <reference types="@types/googlemaps" />

import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';
const TYPES = ["point_of_interest", "park", "zoo", "hospital", "cafe", "bar", "art_gallery", "restaurant", "school", "spa", "stadium"];
const TYPESDISTRRICT = ["sublocality", "sublocality_level_2"];
const RADIUS = 50;
@Injectable({
  providedIn: 'root'
})
export class PlaceGoogleServiceService {
  place: google.maps.places.PlacesService;
  constructor() {
    //для нормальной работы сервиса
    var virtualMap = document.createElement('div');
    this.place = new google.maps.places.PlacesService(virtualMap);
  }
  nearbySearchSortRating(latLang: google.maps.LatLng): Observable<google.maps.places.PlaceResult[]> {
    return Observable.create((observer: Observer<google.maps.places.PlaceResult[]>) => {
      this.place.nearbySearch({ location: latLang, types: TYPES, radius: RADIUS }, (
        (results: google.maps.places.PlaceResult[], status: google.maps.places.PlacesServiceStatus) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            results = results.filter((item) => {
              return item.rating > 3
            }).sort((a, b) => {
              return b.rating - a.rating;
            });
            observer.next(results);
            observer.complete();
          } else {
            console.log('PlaceGoogle service: place failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    })
  }
  searchText(address: string): Observable<google.maps.places.PlaceResult[]> {
    return Observable.create((observer: Observer<google.maps.places.PlaceResult[]>) => {
      this.place.textSearch({ query: address, types: TYPESDISTRRICT, }, (
        (results: google.maps.places.PlaceResult[], status: google.maps.places.PlacesServiceStatus) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('PlaceGoogle service: place failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    })
  }
  getDetail(placeId: string): Observable<google.maps.places.PlaceResult> {
    return Observable.create((observer: Observer<google.maps.places.PlaceResult>) => {
      this.place.getDetails({ placeId: placeId }, (
        (results: google.maps.places.PlaceResult, status: google.maps.places.PlacesServiceStatus) => {
          if (status === google.maps.places.PlacesServiceStatus.OK) {
            observer.next(results);
            observer.complete();
          } else {
            console.log('PlaceGoogle service: place failed due to: ' + status);
            observer.error(status);
          }
        })
      );
    })
  }
}
