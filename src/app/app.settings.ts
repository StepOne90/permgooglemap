import { Injectable } from '@angular/core';
import { Url } from 'url';

@Injectable({
    providedIn: 'root'
})
export class AppSettings {
    SHOWMENU: boolean = true;
    getSettings(): AppSettingsModel {
        return {
            SHOWMENU: this.SHOWMENU,
            MAPSSTYLE: this.MAPSSTYLE,
            MAPTYPE: this.MAPTYPE
        }
    }
    MAPTYPE: MapModel[] = [
        {
            MapDescription: "Место, которое вы воспринимаете как центр жизни Перми, его сердце.",
            MapQuestion: "Где центр Перми?",
            Id: 1,
            Name: "CenterSity",
            MapTypeMarker: TypeMarker.circle,
            MarkerTypes: [{
                Color: 'rgb(248, 168, 48)',
                MarkerId: "1_M",
                TotalCountSender: 1,
                Caption: "Центр города"
            }],
            Color: 'rgb(248, 168, 48)',
            MapDesriptionCaption: "Укажите центр Перми"
        }, {
            MapDescription: "Место, которое вы воспринимаете как центр жизни района, его сердце [где бы вы назначили встречу, откуда начали свою прогулку и т.п.]\
            ",
            MapQuestion: "Где центр вашего района?",
            Id: 2,
            Name: "CenterDistrict",
            MapTypeMarker: TypeMarker.circle,
            MarkerTypes: [{
                Color: 'rgb(242, 102, 0)',
                MarkerId: "2_M",
                TotalCountSender: 1,
                Caption: "Центр района"
            }],
            Color: 'rgb(242, 102, 0)',
            MapDesriptionCaption: "Укажите центр вашего района"
        }, {
            MapDescription: "Общественные места, не связанные с домом и работой, где вы чаще всего бываете.",
            MapQuestion: "Любимые публичные места горожан",
            Id: 3,
            Name: "MorePublicPlace",
            MapTypeMarker: TypeMarker.circle,
            MarkerTypes: [{
                Color: 'rgb(154, 82, 147)',
                MarkerId: "3_M",
                TotalCountSender: 5,
                Caption: "Любимое место"
            }],
            Color: 'rgb(154, 82, 147)',
            MapDesriptionCaption: "Укажите до 5 любимых публичных мест"
        }, {
            MapDescription: "Объекты, которые вызывают у вас наибольшие приятные эмоции.",
            MapQuestion: "Любимые здания и памятники",
            Id: 4,
            Name: "LikePublicPlace",
            MapTypeMarker: TypeMarker.circle,
            MarkerTypes: [{
                Color: 'rgb(45, 225, 252)',
                MarkerId: "4_M",
                TotalCountSender: 5,
                Caption: "Здание",
            }, {
                Color: 'rgb(23, 103, 110)',
                MarkerId: "5_M",
                TotalCountSender: 5,
                Caption: "Памятник"
            }],
            Color: '#17676E',
            MapDesriptionCaption: "Отметьте до 5 зданий и 5 памятников",
            Legend:[
                {
                    Caption:"Здания",
                    Color: 'rgb(45, 225, 252)',
                    MarkerId: "4_M"
                },{
                    Caption:"Памятники",
                    Color: 'rgb(23, 103, 110)',
                    MarkerId: "5_M"
                }
            ]
        }, {
            MapDescription: "Объекты, которые вызывают у вас наибольшие негативные эмоции.",
            MapQuestion: "Самые уродливые здания и памятники",
            Id: 5,
            Name: "DisLikePublicPlace",
            MapTypeMarker: TypeMarker.circle,
            MarkerTypes: [{
                Color: 'rgb(184, 74, 98)',
                MarkerId: "6_M",
                TotalCountSender: 5,
                Caption: "Здание"
            }, {
                Color: 'rgb(90, 39, 173)',
                MarkerId: "7_M",
                TotalCountSender: 5,
                Caption: "Памятник"
            }],
            Color: '#5A27AD',
            MapDesriptionCaption: "Отметьте до 5 зданий и 5 памятников",
            Legend:[
                {
                    Caption:"Здания",
                    Color: 'rgb(184, 74, 98)',
                    MarkerId: "6_M",
                },{
                    Caption:"Памятники",
                    Color: 'rgb(90, 39, 173)',
                    MarkerId: "7_M"
                }
            ]
        }, {
            MapDescription: "Ранжируйте знакомые вам или все районы Перми по безопасности (на дорогах, улицах, во дворах, в подъездах, наличие пьяных,\
            агрессивных компаний, стай бездомных животных и т.п.).",
            MapQuestion: "Безопасные и опасные районы города",
            Id: 6,
            Name: "ProtectedDistrict",
            MapTypeMarker: TypeMarker.poligon,
            MarkerTypes: [{
                MarkerId: "8_D",
                PositivTooltip: "Безопасный район",
                NegativTooltip: "Опасный район"
            }],
            Color: '#3880C2',
            MapDesriptionCaption: "Оцените районы Перми по ощущению безопасности",
            Legend:[
                {
                    Caption:"Безопасные",
                    Color:"green"
                },
                {
                    Caption:"Опасные",
                    Color:"red"
                },
                {
                    Caption:"Нейтральные",
                    Color:"#88788C"
                },

            ]            
        }, {
            MapDescription: "Ранжируйте знакомые вам или все районы Перми по уровню благоустройства (качество дорог и тротуаров, их чистота и сухость, \
            освещение на улицах, наличие лавок, урн для мусора, удобных парков и скверов и т.д.).",
            MapQuestion: "Комфортные и некомфортные районы города",
            Id: 7,
            Name: "ComfortDistrict",
            MapTypeMarker: TypeMarker.poligon,
            MarkerTypes: [{
                MarkerId: "9_D",
                PositivTooltip: "Комфортный район",
                NegativTooltip: "Некомфортный район"
            }],
            Color: '#D92D57',
            MapDesriptionCaption: "Оцените районы Перми по ощущению комфорта",
            Legend:[
                {
                    Caption:"Комфортные",
                    Color:"green"
                },
                {
                    Caption:"Некомфортные",
                    Color:"red"
                },
                {
                    Caption:"Нейтральные",
                    Color:"#88788C"
                },

            ]  
        }, {
            MapDescription: "Ранжируйте районы Перми по желанию жить в них больше или меньше всего. Кроме этого можно обозначить конкретное место, улицу или микрорайон,\
            куда бы вам хотелось переехать.",
            MapQuestion: "Районы, где хочется жить больше и меньше всего",
            Id: 8,
            Name: "LiveDistrict",
            MapTypeMarker: TypeMarker.poligon,
            MarkerTypes: [{
                MarkerId: "10_D",
                PositivTooltip: "Хочу жить здесь",
                NegativTooltip: "Не хочу жить здесь"
            }],
            Color: '#7EAD7C',
            MapDesriptionCaption: "Оцените районы Перми по желанию в них жить",
            Legend:[
                {
                    Caption:"Хочется жить",
                    Color:"green"
                },
                {
                    Caption:"Не хочется жить",
                    Color:"red"
                },
                {
                    Caption:"Нейтральные",
                    Color:"#88788C"
                },

            ] 
        }
    ]

    MAPSSTYLE: google.maps.MapTypeStyle[] = [
        {
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "elementType": "geometry.stroke",
            "stylers": [
                {
                    "color": "#c0c0c0"
                },
                {
                    "lightness": -40
                },
                {
                    "visibility": "on"
                }
            ]
        },
        {
            "elementType": "labels.icon",
            "stylers": [
                {
                    "visibility": "off"
                }
            ]
        },
        {
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "elementType": "labels.text.stroke",
            "stylers": [
                {
                    "color": "#f5f5f5"
                }
            ]
        },
        {
            "featureType": "administrative.land_parcel",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#bdbdbd"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "poi",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "poi.park",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "road",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#757575"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#dadada"
                }
            ]
        },
        {
            "featureType": "road.highway",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#616161"
                }
            ]
        },
        {
            "featureType": "road.local",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        },
        {
            "featureType": "transit.line",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#e5e5e5"
                }
            ]
        },
        {
            "featureType": "transit.station",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#eeeeee"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [
                {
                    "color": "#c9c9c9"
                }
            ]
        },
        {
            "featureType": "water",
            "elementType": "labels.text.fill",
            "stylers": [
                {
                    "color": "#9e9e9e"
                }
            ]
        }
    ]
}
export enum TypeMarker { "circle", "poligon" }

export interface AppSettingsModel {
    SHOWMENU: boolean;
    MAPSSTYLE: google.maps.MapTypeStyle[]
    MAPTYPE: MapModel[]
}
export interface MapModel {
    Name: string;
    MapDescription: string;
    MapQuestion: string;
    Id: number;
    MapTypeMarker: TypeMarker;
    MarkerTypes: MarkerTypeModel[];
    Color: string;
    MapDesriptionCaption: string;
    Legend?:LegendModel[];
}

export interface MarkerTypeModel {
    MarkerId: string,
    Caption?: string,
    Color?: string;
    IconUrl?: Url;
    PositivTooltip?: string;
    NegativTooltip?: string;
    TotalCountSender?: number;    
}
export interface LegendModel {
    Caption: string;
    Color: string;
    MarkerId?:string;
}