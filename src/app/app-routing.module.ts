import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GoogleMapComponent } from './google-map/google-map.component';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  { path: 'map/:mapType', component: GoogleMapComponent },
  { path: 'map', component: GoogleMapComponent },
  { path: 'about', component: AboutComponent },
  { path: '', redirectTo: "map" ,pathMatch: 'full'},
  { path: '**', redirectTo: "map" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
