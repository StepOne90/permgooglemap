import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-map-info-modal',
  templateUrl: './map-info-modal.component.html',
  styleUrls: ['./map-info-modal.component.css']
})
export class MapInfoModalComponent implements OnInit {

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }
  goToMap(){
    this.bsModalRef.hide();
  }

}
