import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapInfoModalComponent } from './map-info-modal.component';

describe('MapInfoModalComponent', () => {
  let component: MapInfoModalComponent;
  let fixture: ComponentFixture<MapInfoModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapInfoModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapInfoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
