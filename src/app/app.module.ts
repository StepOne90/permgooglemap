import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { GoogleMapComponent } from './google-map/google-map.component'
import { AgmCoreModule } from '@agm/core';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome'
import { AngularFireModule } from '@angular/fire';
import { environment } from 'src/environments/environment';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { MapInfoModalComponent } from './map-info-modal/map-info-modal.component';
import { ModalModule, TooltipModule } from 'ngx-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { AddPlaceBidComponent } from './add-place-bid/add-place-bid.component';
import { LoginUserModalComponent } from './services/account-service/login-user-modal/login-user-modal.component';
import { AboutComponent } from './about/about.component';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  declarations: [
    GoogleMapComponent,
    AppComponent,
    MapInfoModalComponent,
    AddPlaceBidComponent,
    LoginUserModalComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BsDropdownModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAznXlo3taLyUFgiynPFTUF_Fqe61eNGkE'
    }),
    Angular2FontawesomeModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [MapInfoModalComponent, AddPlaceBidComponent,LoginUserModalComponent]
})
export class AppModule { }