/// <reference types="@types/googlemaps" />
import { Component, OnInit, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { PlaceGoogleServiceService } from '../services/place-google-service/place-google-service.service'
import { DALServise, PlaceSelectModel, SelectUsersModel, Marker } from '../services/DAL-Servise/dalservise.service';
import { AppSettings, LegendModel, MapModel, TypeMarker, MarkerTypeModel } from '../app.settings'
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MapInfoModalComponent } from '../map-info-modal/map-info-modal.component'
import { AccountService, UsreInfoModel } from '../services/account-service/account.service'
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import {fromEvent} from 'rxjs';

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit {
  @Output() isOpenedLink = new EventEmitter<boolean>();
  placesSelected: PlaceSelectModel[];
  centerMap: google.maps.LatLng = new google.maps.LatLng(57.995830, 56.253922);
  controlPositionOption: google.maps.StreetViewControlOptions = {
    position: google.maps.ControlPosition.RIGHT_CENTER
  };
  zoomControlOptions: google.maps.ZoomControlOptions = {
    position: google.maps.ControlPosition.RIGHT_CENTER
  }
  zoomPlace: number = 13;
  zoomDistrict: number = 11;
  zoom: number = this.zoomPlace;
  minZoom: number = 13;
  markers: Marker[] = [];
  gestureHandling: string = 'none';
  placeService: google.maps.places.PlacesService;
  appSettings: AppSettings;
  mapType: MapModel;
  bsModalRef: BsModalRef;
  polygons: any = [];
  isAddPlace: boolean = false;
  selectAddMrkerType: MarkerTypeModel = null;
  svrdlovskPlaceName: string = "ChIJ4TO55LLD6EMRkFGPZlKxvSE";
  user: UsreInfoModel = null;
  map: any;
  showPanoram: boolean = false;
  showDescription = true;
  @ViewChild('inputSearch') inputSearch: ElementRef;
  @ViewChild('mapGoogle') mapGoogle: ElementRef;

  constructor(private placeSerice: PlaceGoogleServiceService, private dalService: DALServise
    , private appSetting: AppSettings, private route: ActivatedRoute, private modalService: BsModalService
    , private accountService: AccountService) {
    this.appSettings = appSetting;
  }
  //для поиска по карте
  public onMapReady(map) {
    let _self = this;
    this.map = map;
    var input: HTMLInputElement = (this.inputSearch.nativeElement as HTMLInputElement);
    let maps = window['google'].maps;
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);
    autocomplete.addListener('place_changed', () => {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);
      }
      if (!_self.selectAddMrkerType) return;
      let marker = _self.getNewMarker(place.geometry.location, _self.selectAddMrkerType.Color, true);
      this.getPlaceInfo(marker).subscribe(
        result => {
          marker.description = result;
        }
      );
      _self.markers.push(marker);
    })
  }
  setCenterMap() {
    this.centerMap = new google.maps.LatLng(57.995830, 56.253922);
  }
  isMapTypewithSelect(marker: Marker) {
    if (!this.mapType || !this.mapType.MarkerTypes) return false;
    let selectMapType = this.mapType.MarkerTypes.filter(type => {
      return type.MarkerId == marker.typeId
    })
    return selectMapType.length > 0;
  }
  initMarkers(typeId: string, colorMarker: string) {
    //this.markers = [];
    this.dalService.GetCountMarkersBymarkerType(typeId).subscribe(
      values => {
        values.forEach(value => {
          this.dalService.GetSenderUsersByMarker(value.DocId, "placeSelect").subscribe(result => {
            let selectplace = value;
            selectplace.SelectUsers = result;
            let marker: Marker = {
              lat: selectplace.Lat,
              lng: selectplace.Lng,
              contentInfoWindowPlace: null,
              selectUsers: selectplace.SelectUsers,
              radius: 50 + (10 * selectplace.SelectUsers.length),
              docId: selectplace.DocId,
              isOpenInfoWindow: false,
              color: colorMarker,
              typeId: typeId,
              name: value.Name,
              description: value.Description
            }
            if (this.isMapTypewithSelect(marker))
              this.markers.push(marker);
          }
          )
        })
      }
    );
  }
  initDistrict(value: google.maps.places.PlaceResult[]) {
    //this.markers = [];
    let _self = this;
    let marketTypes = this.mapType.MarkerTypes;
    if (marketTypes.length > 1) return;
    value.forEach(element => {
      /*проблема с свердловским р-ом т.к. он разбросан как попало*/
      let center = element.geometry.location;
      if (element.place_id == this.svrdlovskPlaceName)
        center = new google.maps.LatLng(57.966033, 56.277788);
      ////
      let m = this.getNewMarker(center, this.mapType.Color, false, "0");
      m.contentInfoWindowDistrict = element.formatted_address.split(',')[0];
      m.placeId = element.place_id;
      this.dalService.GetDistrictPolygon(element.formatted_address, element.place_id).subscribe(result => {
        m.districtPolygons = result;
        _self.markers.push(m);
        this.dalService.GetDistrictSelectUsers(marketTypes[0].MarkerId, m.placeId).subscribe(rCount => {
          m.selectUsers = rCount.SelectUsers;
          m.docId = rCount.docId;
          // _self.markers.push(m);
        });
      })
    });
  }
  ngOnInit() {
    fromEvent(this.mapGoogle.nativeElement, 'click')
      .subscribe((res: any) => {
        this.showDescription=false
      });
    this.route
      .params
      .subscribe(params => {
        this.showDescription = true;
        this.markers = [];
        //this.setCenterMap();
        let mapTypes = this.appSetting.getSettings().MAPTYPE.filter(type => { return type.Name == params.mapType });
        if (mapTypes && mapTypes.length > 0) {
          this.isOpenedLink.emit(true);
          this.mapType = mapTypes[0];
          switch (this.mapType.MapTypeMarker) {
            case TypeMarker.circle:
              this.zoom = this.zoomPlace;
              this.mapType.MarkerTypes.forEach(element => {
                this.initMarkers(element.MarkerId, element.Color);
              });
              if (this.isAddPlace) {
                this.selectAddMrkerType = this.mapType.MarkerTypes.length == 1 ? this.mapType.MarkerTypes[0] : null;
              }
              break;
            case TypeMarker.poligon:
              this.zoom = this.zoomDistrict;
              this.placeSerice.searchText("Районы перми").subscribe(
                value => {
                  this.initDistrict(value);
                }
              )
              break;
            default:
              break;
          }

        } else {
          this.bsModalRef = this.modalService.show(MapInfoModalComponent, { 'class': 'modal-custom' });
        }
      });
  }
  markerClick(marker: Marker) {
    marker.isOpenInfoWindow = !marker.isOpenInfoWindow;
  }
  //информация по координатам
  getPlaceInfo(marker: Marker): Observable<string> {
    if (marker.contentInfoWindowPlace)
      return;
    var latLng = new google.maps.LatLng(marker.lat, marker.lng);
    let resultPlace: Array<google.maps.places.PlaceResult>;
    return this.placeSerice.nearbySearchSortRating(latLng).pipe(map(value => {
      let html = "<table style='max-width: 200px;'>";
      value.forEach(element => {
        html += "<tr>";
        html += "<td>" + element.name + "</td>";
        html += "<td>" + element.rating + "</td>";
        html += "</tr>";
      });
      html += "</table>";
      return html
    }))
    //subscribe(
    //  value => {
    //    observer.next(value);
    //    observer.complete();
    //    // marker.contentInfoWindowPlace = value;
    //  }, erroe => { observer.error("ошибка получения данных по месту") }
    //);
  }
  //добавление голоса к месту
  clickedAddSenderPlace(marker: Marker): void {
    let user = this.accountService.getUser(true);
    if (!user.Uid) return;
    this.dalService.AddSenderUserByPLace(marker.docId, user, "placeSelect").subscribe((result) => {
      marker.selectUsers.push(result);
    })
  }
  //добавление голоса к району  
  isBlockSenderDistrict: boolean = false;
  clickedAddSenderDistrict(marker: Marker, resultSelect: boolean): void {
    let user = this.accountService.getUser(true);
    if (!user.Uid) return;
    let marketTypes = this.mapType.MarkerTypes;
    if (marketTypes.length > 1 || this.isBlockSenderDistrict) return;
    this.isBlockSenderDistrict = true;
    this.dalService.AddSenderDistrictUserShared(marker.docId, user, marker.placeId, marketTypes[0].MarkerId, resultSelect).subscribe(result => {
      marker.selectUsers.push({ Uid: user.Uid, ResultSelect: resultSelect, DataIsert: new Date().getTime() });
      this.isBlockSenderDistrict = false;
    },
      (error) => { console.error(error); this.isBlockSenderDistrict = false; });
  }
  getNewMarker(latLng: google.maps.LatLng, color: string, isOpenInfoWindow: boolean = true, label: string = "1"): Marker {
    return {
      radius: 50,
      selectUsers: [],
      lat: latLng.lat(),
      lng: latLng.lng(),
      contentInfoWindowPlace: null,
      isOpenInfoWindow: isOpenInfoWindow,
      color: color,
      docId: null,
      typeId: null,
      name: "",
      description: ""
    }
  }
  //клик по карте
  mapClicked($event: MouseEvent) {
    let user = this.accountService.getUser()
    if (!this.isAddPlace || !user.IsAdmin || !this.selectAddMrkerType || this.mapType.MapTypeMarker != TypeMarker.circle) return;
    if (!this.selectAddMrkerType) return;
    let marker = this.getNewMarker(new google.maps.LatLng($event.coords.lat, $event.coords.lng), this.selectAddMrkerType.Color);
    this.getPlaceInfo(marker).subscribe(
      result => {
        marker.description = result;
      }
    );
    this.markers.push(marker);
  }
  addNewPlace(marker: Marker): void {
    if (this.selectAddMrkerType)
      this.dalService.AddNewPlaceSelectWithMarker(marker, this.selectAddMrkerType.MarkerId, this.accountService.getUser()).then(docId => {
        marker.docId = docId;
      });
  }
  //Начать добавление новых мест
  activateAddNewPlace($event: MouseEvent) {
    if (!this.accountService.getUser().IsAdmin) return;
    this.isAddPlace = !this.isAddPlace;
    if (this.mapType.MarkerTypes.length == 1) {
      this.selectAddMrkerType = this.mapType.MarkerTypes[0];
    }
  }
  getStatisticsUserSelect(usersSelect: SelectUsersModel[]) {
    let result = [];
    //result.push({
    //  label: "Всего проголосовало",
    //  count: usersSelect.length,
    //  color: '#000000'
    //});
    result.push({
      label: this.mapType.MarkerTypes[0].PositivTooltip,
      count: usersSelect.filter(u => { return u.ResultSelect }).length,
      color: 'green'
    });
    result.push({
      label: this.mapType.MarkerTypes[0].NegativTooltip,
      count: usersSelect.filter(u => { return !u.ResultSelect }).length,
      color: 'red'
    });
    return result;
  }
  getLablemarker(marker: Marker): string {
    if (marker.countAddUserSelect) return marker.countAddUserSelect.toString();
    return marker.selectUsers.length.toString();
  }
  getColorDistrict(marker: Marker): string {
    let positivSenders = marker.selectUsers.filter((user) => { return user.ResultSelect }).length;
    let negativSenders = marker.selectUsers.length - positivSenders;
    if (marker.selectUsers.length == 0 || positivSenders == negativSenders)
      return '#88788C';
    if (positivSenders > negativSenders)
      return 'green';
    else return 'red'
  }
  getLegendMarker(): LegendModel[] {
    if (!this.mapType.Legend)
      return [];
    return this.mapType.Legend;
  }
  selectAddPlaceType(selectMarkerType: MarkerTypeModel) {
    this.selectAddMrkerType = selectMarkerType;
  }
  getRadiusMarker(marker: Marker): number {
    let radius = marker.selectUsers.length * 10;
    radius = radius > 200 ? 200 : radius;
    radius = radius < 50 ? 50 : radius;
    return radius;
  }
  getOpacityDistrict(marker: Marker): number {
    if (marker.selectUsers.length == 0)
      return 0.5;
    let positivSenders = marker.selectUsers.filter((user) => { return user.ResultSelect }).length;
    let negativSenders = marker.selectUsers.length - positivSenders;
    if (positivSenders == negativSenders)
      return 0.5;
    if (positivSenders > negativSenders) {
      let perc = positivSenders / marker.selectUsers.length;
      return perc / 1.8;
    } else {
      let perc = negativSenders / marker.selectUsers.length;
      return perc / 1.8;
    }

  }
  //проверка возможности добавить голос к месту
  isActiveAddSender(marker: Marker): boolean {
    let _self = this;
    if (this.accountService.getUser().IsAdmin)
      return true;
    let haveCountSend = this.markers.filter(function (m) {
      if (m.typeId != marker.typeId) return;
      let isSend = m.selectUsers.filter(function (selectUser) {
        return selectUser.Uid == _self.accountService.getUser().Uid;
      })
      return isSend.length > 0;
    })
    let selectMarker = this.mapType.MarkerTypes.filter(type => {
      return type.MarkerId == marker.typeId
    })[0];
    if (haveCountSend.length >= selectMarker.TotalCountSender) {
      return false;
    }
    let isSelect = marker.selectUsers.filter(user => {
      return user.Uid == this.accountService.getUser().Uid;
    })
    if (isSelect.length > 0)
      return false;
    return true;
  }
  isActiveAddSenderPlygon(marker: Marker): boolean {
    let _self = this;
    if (this.accountService.getUser().IsAdmin)
      return true;
    let isSelect = marker.selectUsers.filter(user => {
      return user.Uid == this.accountService.getUser().Uid;
    })
    if (isSelect.length > 0)
      return false;
    return true;
  }
  saveEditMarker(marker: Marker) {
    marker.isEdit = false;
    this.dalService.UpdateMetaPlace(marker.docId, marker);
  }
  infoWindowClose(marker: Marker, i: number) {
    marker.isOpenInfoWindow = false;
    if (this.mapType.MapTypeMarker == TypeMarker.poligon) return;
    marker.isEdit = false;
    if (!marker.docId) {
      this.markers.splice(i, 1);
    }
  }
  isWindowMobile(): boolean {
    return window.innerWidth < 768;
  }
  //панорама
  openStreetView(marker: Marker) {
    let center = { lat: marker.lat, lng: marker.lng };
    let _self = this;
    var panorama = new google.maps.StreetViewPanorama(
      document.getElementById('pano'), {
        position: center,
        enableCloseButton: true,
        pov: {
          heading: 34,
          pitch: 10
        }
      });
    this.showPanoram = true;
    this.map.setStreetView(panorama);
    google.maps.event.addListener(this.map.getStreetView(), 'visible_changed', function () {
      _self.showPanoram = !_self.showPanoram;
    });
  }
  removeMarker(marker: Marker, i: number): void {
    if (!this.accountService.getUser().IsAdmin || i < 0)
      return;
    if (confirm("Вы уверены что хотите удалить место " + (marker.name || ""))) {
      this.dalService.RemovePlace(marker)
      this.markers.splice(i, 1);
    }
  }
}
